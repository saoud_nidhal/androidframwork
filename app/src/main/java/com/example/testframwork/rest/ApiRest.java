package com.example.testframwork.rest;

import com.example.testframwork.Constants;
import com.example.testframwork.pojo.GetCityresponse;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;


public interface ApiRest {

    @GET(Constants.CIT_ENDPOINT)
    Observable<Response<GetCityresponse>> getCityList();

}
