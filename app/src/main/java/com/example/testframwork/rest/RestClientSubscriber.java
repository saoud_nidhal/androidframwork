package com.example.testframwork.rest;

import com.example.testframwork.pojo.GetCityresponse;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class RestClientSubscriber {

    private final ApiRest networkService;

    public RestClientSubscriber(ApiRest networkService) {
        this.networkService = networkService;
    }

    public Subscription getCountryList(final Callback<GetCityresponse> callback)
    {
        return networkService.getCityList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends Response<GetCityresponse>>>() {
                    @Override
                    public Observable<? extends Response<GetCityresponse>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<Response<GetCityresponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError());

                    }

                    @Override
                    public void onNext(Response<GetCityresponse> cityListResponse) {
                        callback.onSuccess(cityListResponse);

                    }
                });
    }
}
