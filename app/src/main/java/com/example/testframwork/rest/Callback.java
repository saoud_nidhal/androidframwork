package com.example.testframwork.rest;



import retrofit2.Response;


public interface Callback<T> {

    void onSuccess(Response<T> response);

    void onError(NetworkError networkError);
}
