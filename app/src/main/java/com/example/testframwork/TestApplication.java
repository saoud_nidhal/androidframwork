package com.example.testframwork;

import android.app.Application;


import com.example.testframwork.module.AppModule;
import com.example.testframwork.module.DaggerNetCompenent;
import com.example.testframwork.module.NetCompenent;
import com.example.testframwork.module.NetworkModule;



public class TestApplication extends Application {
    NetCompenent deps;

    @Override
    public void onCreate() {
        super.onCreate();
        deps = DaggerNetCompenent.builder().appModule(new AppModule(this)).

                networkModule(new NetworkModule()).build();

    }

    public NetCompenent getDeps() {
        return deps;
    }

}
