package com.example.testframwork.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.testframwork.R;
import com.example.testframwork.TestApplication;
import com.example.testframwork.adapter.CountryAdapter;
import com.example.testframwork.event.ClickCoutryitemEvent;
import com.example.testframwork.helpers.Storage;
import com.example.testframwork.rest.RestClientSubscriber;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


import rx.subscriptions.CompositeSubscription;


public class MainActivity extends MvpActivity<MainView, MainPresenter> implements MainView {



    RecyclerView mList;

    ProgressBar mProgress;
    private CompositeSubscription subscriptions;
    CountryAdapter mAdapterCountry;
    List<String> mListOfCountry;

    @Inject
    RestClientSubscriber service;
    @Inject
    Storage storageToUse;

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mList = (RecyclerView)findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        subscriptions = new CompositeSubscription();

        ((TestApplication) getApplication()).getDeps().inject(this);
        initRecycleView();
        presenter.getLisfCountry(service, storageToUse, subscriptions);
        presenter.setAlpha(0.5f);

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        subscriptions.unsubscribe();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void showProgress() {

        if (mProgress != null)
            mProgress.setVisibility(View.VISIBLE);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ClickCoutryitemEvent event) {
        Toast.makeText(this, event.getmCoutry() + " position " + event.getmPositon(), Toast.LENGTH_LONG).show();
    }


    @Override
    public void hideProgress() {
        if (mProgress != null)
            mProgress.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

    }

    @Override
    public void showResult(List<String> _mListOfCoutry) {
        mListOfCountry.clear();
        mListOfCountry.addAll(_mListOfCoutry);
        mAdapterCountry.notifyDataSetChanged();
    }

    private void initRecycleView() {
        mListOfCountry = new ArrayList<>();
        mAdapterCountry = new CountryAdapter(mListOfCountry);
        mList.setHasFixedSize(true);
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setAdapter(mAdapterCountry);

    }
}
