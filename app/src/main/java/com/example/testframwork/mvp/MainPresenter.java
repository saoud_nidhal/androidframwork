package com.example.testframwork.mvp;

import android.support.annotation.FloatRange;
import android.util.Log;


import com.example.testframwork.helpers.Storage;
import com.example.testframwork.pojo.GetCityresponse;
import com.example.testframwork.rest.NetworkError;
import com.example.testframwork.rest.RestClientSubscriber;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;


import retrofit2.Response;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public class MainPresenter extends MvpBasePresenter<MainView> {


    public MainPresenter(){


    }
    /*  @RequiresPermission(allOf = {
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION})*/
    public void getLisfCountry(RestClientSubscriber service,final Storage storageToUse,CompositeSubscription subscriptions){
        getView().showProgress();
        Subscription subscription = service.getCountryList(new com.example.testframwork.rest.Callback<GetCityresponse>() {
            @Override
            public void onSuccess(Response<GetCityresponse> response) {
                getView().hideProgress();
                if(response!=null&&response.body()!=null){
                    StringBuilder mStringBuilderMessage= new StringBuilder();
                    mStringBuilderMessage.append(response.body().getMsg());
                    mStringBuilderMessage.append(" ");
                    mStringBuilderMessage.append("Country Number");
                    mStringBuilderMessage.append(" ");
                    mStringBuilderMessage.append(response.body().getCountry().size());
                    getView().showMessage(mStringBuilderMessage.toString());
                    storageToUse.setApiKey(response.body().getMsg());
                    getView().showResult(response.body().getCountry());
                }


            }

            @Override

            public void onError(NetworkError networkError) {
                getView().hideProgress();
                getView().showMessage("erreur");
            }
        });

        subscriptions.add(subscription);
    }


    public void setAlpha(@FloatRange(from = 0.0, to = 1.0) float newAlpha) {
        Log.e(MainPresenter.class.getCanonicalName(), "Test for Android annotation "+newAlpha);

    }

}
