package com.example.testframwork.mvp;


import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;


public interface MainView extends MvpView {

    public void showProgress();
    public void hideProgress();
    public void showMessage(String message);
    public void showResult(List<String> mListOfCoutry);
}
