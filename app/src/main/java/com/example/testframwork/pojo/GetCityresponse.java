
package com.example.testframwork.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCityresponse {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("country")
    @Expose
    private List<String> country = null;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<String> getCountry() {
        return country;
    }

    public void setCountry(List<String> country) {
        this.country = country;
    }

}
