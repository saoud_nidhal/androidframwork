package com.example.testframwork.module;

import android.app.Application;

import com.example.testframwork.Constants;
import com.example.testframwork.helpers.Storage;
import com.example.testframwork.rest.ApiRest;
import com.example.testframwork.rest.RestClientSubscriber;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;



@Module
public class NetworkModule {

    private static final int CONNECTION_TIMEOUT = 30;

    // Constructor needs one parameter to instantiate.
    public NetworkModule() {

    }

    // Dagger will only look for methods annotated with @Provides
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    // Application reference must come from AppModule.class
    Storage providesStorage(Application application) {
        return new Storage(application);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    HttpLoggingInterceptor provideInerceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    OkHttpClient provideOkHttpClient(Cache cache, HttpLoggingInterceptor interceptor) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.cache(cache);
        okHttpClient.addInterceptor(interceptor);
        return okHttpClient.build();
    }


    @Provides
    @Singleton
    @SuppressWarnings("unused")
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ApiRest providesNetworkService(
            Retrofit retrofit) {
        return retrofit.create(ApiRest.class);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public RestClientSubscriber providesService(
            ApiRest networkService) {
        return new RestClientSubscriber(networkService);
    }
}
