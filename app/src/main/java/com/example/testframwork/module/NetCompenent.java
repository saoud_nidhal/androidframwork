package com.example.testframwork.module;

import com.example.testframwork.mvp.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class,NetworkModule.class})
public interface NetCompenent {
    void inject(MainActivity activity);
}

