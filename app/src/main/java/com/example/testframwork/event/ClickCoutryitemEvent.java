package com.example.testframwork.event;



public class ClickCoutryitemEvent {
    String mCoutry;
    int mPositon;

    public int getmPositon() {
        return mPositon;
    }

    public String getmCoutry() {
        return mCoutry;
    }

    public ClickCoutryitemEvent(String country, int position) {
        this.mCoutry = country;
        this.mPositon = position;
    }
}
