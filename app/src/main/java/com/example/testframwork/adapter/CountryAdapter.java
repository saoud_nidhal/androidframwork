package com.example.testframwork.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.testframwork.R;
import com.example.testframwork.viewholder.CoutryViewHolder;

import java.util.List;



public class CountryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<String> items;

    public CountryAdapter(List<String> data) {
        this.items = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country, parent, false);

        return new CoutryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String country = items.get(position);
        ((CoutryViewHolder) holder).bindViewHolder(country);
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }
}
